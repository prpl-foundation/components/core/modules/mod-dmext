/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dmext_priv.h"

static const char* dmext_redirect_get_reference(amxc_var_t* ref) {
    const char* ref_param = "Reference";
    when_true(amxc_var_is_null(ref), exit);

    ref_param = GET_CHAR(ref, NULL);

exit:
    return ref_param;
}

static amxd_status_t dmext_redirect_read(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    amxd_status_t redirect_status = amxd_status_ok;
    amxd_status_t status = amxd_status_ok;
    const amxc_var_t* ref = NULL;
    amxd_object_t* ref_obj = NULL;
    amxd_dm_t* dm = NULL;
    amxc_var_t local_obj;
    amxc_var_t* ref_name = (amxc_var_t*) priv;
    const char* ref_path = dmext_redirect_get_reference(ref_name);

    amxc_var_init(&local_obj);

    when_null(object, exit);
    ref = amxd_object_get_param_value(object, ref_path);

    dm = amxd_object_get_dm(object);
    when_null(dm, exit);

    amxc_var_set_type(retval, AMXC_VAR_ID_HTABLE);
    ref_obj = amxd_dm_findf(dm, "%s", GET_CHAR(ref, NULL));
    if(ref_obj != NULL) {
        redirect_status = amxd_action_object_read(ref_obj, param, reason, args, retval, priv);
    }
    when_false(redirect_status == amxd_status_parameter_not_found ||
               redirect_status == amxd_status_ok, exit);

    if(redirect_status != amxd_status_ok) {
        amxc_var_set_type(retval, AMXC_VAR_ID_HTABLE);
    }

    status = amxd_action_object_read(object, param, reason, args, &local_obj, priv);
    when_false(status == amxd_status_parameter_not_found ||
               status == amxd_status_ok, exit);

    amxc_var_for_each(local_param, &local_obj) {
        amxc_var_set_key(retval, amxc_var_key(local_param), local_param, AMXC_VAR_FLAG_UPDATE);
    }

    if((status == amxd_status_parameter_not_found) &&
       ( redirect_status == amxd_status_parameter_not_found)) {
        status = amxd_status_parameter_not_found;
        amxc_var_clean(retval);
    } else {
        status = amxd_status_ok;
    }

exit:
    amxc_var_clean(&local_obj);
    return status;
}

static amxd_status_t dmext_redirect_write(amxd_object_t* object,
                                          amxd_param_t* param,
                                          amxd_action_t reason,
                                          const amxc_var_t* const args,
                                          amxc_var_t* const retval,
                                          void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t local_obj;
    amxc_var_t* local_values = NULL;
    amxc_var_t* redirect_values = NULL;
    const amxc_var_t* ref = NULL;
    amxd_object_t* ref_obj = NULL;
    amxd_dm_t* dm = NULL;
    amxc_var_t* ref_name = (amxc_var_t*) priv;
    const char* ref_path = dmext_redirect_get_reference(ref_name);
    const amxc_htable_t* pt = NULL;

    amxc_var_init(&local_obj);

    amxc_var_copy(&local_obj, args);
    local_values = GET_ARG(&local_obj, "parameters");
    amxc_var_set_type(local_values, AMXC_VAR_ID_HTABLE);
    redirect_values = GET_ARG(args, "parameters");

    amxd_object_for_each(parameter, it, object) {
        amxd_param_t* p = amxc_container_of(it, amxd_param_t, it);
        amxc_var_t* value = GET_ARG(redirect_values, amxd_param_get_name(p));
        if(value != NULL) {
            amxc_var_take_it(value);
            amxc_var_set_key(local_values, amxd_param_get_name(p), value, AMXC_VAR_FLAG_DEFAULT);
        }
    }

    status = amxd_action_object_write(object, param, reason, &local_obj, retval, priv);

    ref = amxd_object_get_param_value(object, ref_path);
    dm = amxd_object_get_dm(object);
    when_null_status(dm, exit, status = amxd_status_unknown_error);

    pt = amxc_var_constcast(amxc_htable_t, redirect_values);
    when_true(amxc_htable_is_empty(pt), exit);
    ref_obj = amxd_dm_findf(dm, "%s", GET_CHAR(ref, NULL));
    if(ref_obj != NULL) {
        status = amxd_action_object_write(ref_obj, param, reason, args, retval, priv);
    } else {
        status = amxd_status_parameter_not_found;
    }

exit:
    amxc_var_clean(&local_obj);
    return status;
}

static amxd_status_t dmext_redirect_list(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* ref = NULL;
    amxd_object_t* ref_obj = NULL;
    amxd_dm_t* dm = NULL;
    amxc_var_t* param_names = NULL;
    amxc_var_t* ref_name = (amxc_var_t*) priv;
    const char* ref_path = dmext_redirect_get_reference(ref_name);

    when_null(object, exit);
    ref = amxd_object_get_param_value(object, ref_path);

    dm = amxd_object_get_dm(object);
    when_null(dm, exit);

    status = amxd_action_object_list(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    ref_obj = amxd_dm_findf(dm, "%s", GET_CHAR(ref, NULL));
    when_null(ref_obj, exit);

    status = amxd_status_ok;
    param_names = GET_ARG(retval, "parameters");
    when_null(param_names, exit);
    amxd_object_for_each(parameter, it, ref_obj) {
        amxd_param_t* p = amxc_container_of(it, amxd_param_t, it);
        if(amxd_object_get_param_def(object, amxd_param_get_name(p)) != NULL) {
            continue;
        }
        amxc_var_add(cstring_t, param_names, amxd_param_get_name(p));
    }

exit:
    return status;
}

static amxd_status_t dmext_redirect_describe(amxd_object_t* object,
                                             amxd_param_t* param,
                                             amxd_action_t reason,
                                             const amxc_var_t* const args,
                                             amxc_var_t* const retval,
                                             void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* ref = NULL;
    amxc_var_t ref_descr;
    amxd_object_t* ref_obj = NULL;
    amxd_dm_t* dm = NULL;
    amxc_var_t* params_descr = NULL;
    amxc_var_t* ref_params_descr = NULL;
    amxc_var_t* ref_name = (amxc_var_t*) priv;
    const char* ref_path = dmext_redirect_get_reference(ref_name);

    amxc_var_init(&ref_descr);

    when_null(object, exit);
    ref = amxd_object_get_param_value(object, ref_path);

    dm = amxd_object_get_dm(object);
    when_null(dm, exit);

    status = amxd_action_object_describe(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    ref_obj = amxd_dm_findf(dm, "%s", GET_CHAR(ref, NULL));
    when_null(ref_obj, exit);

    status = amxd_status_ok;
    params_descr = GET_ARG(retval, "parameters");
    when_null(params_descr, exit);

    status = amxd_action_object_describe(ref_obj, param, reason, args, &ref_descr, priv);
    when_failed(status, exit);
    ref_params_descr = GET_ARG(&ref_descr, "parameters");

    amxc_var_for_each(vp, ref_params_descr) {
        amxc_var_t* pattr = NULL;
        if(GET_ARG(params_descr, amxc_var_key(vp)) != NULL) {
            continue;
        }
        // Set persistent attribute to false when following reference
        pattr = GETP_ARG(vp, "attributes.persistent");
        amxc_var_set(bool, pattr, false);
        amxc_var_set_key(params_descr, amxc_var_key(vp), vp, AMXC_VAR_FLAG_DEFAULT);
    }

exit:
    amxc_var_clean(&ref_descr);
    return status;
}

static amxd_action_fn_t redirect_fns[] = {
    NULL,                    // any
    NULL,                    // param_read
    NULL,                    // param_write
    NULL,                    // param_validate
    NULL,                    // param describe
    NULL,                    // param_destroy

    dmext_redirect_read,     // object_read
    dmext_redirect_write,    // object write
    NULL,                    // object_validate
    dmext_redirect_list,     // object_list
    dmext_redirect_describe, // object_describe
    NULL,                    // object_tree
    NULL,                    // object_add_inst
    NULL,                    // object_del_inst
    NULL,                    // object_destroy
    NULL,                    // add_mib
};

amxd_status_t _follow_reference(amxd_object_t* object,
                                amxd_param_t* param,
                                amxd_action_t reason,
                                const amxc_var_t* const args,
                                amxc_var_t* const retval,
                                void* priv) {
    amxd_status_t status = amxd_status_ok;

    when_null_status(redirect_fns[reason],
                     exit,
                     status = amxd_status_function_not_implemented);

    return redirect_fns[reason](object, param, reason, args, retval, priv);

exit:
    return status;
}