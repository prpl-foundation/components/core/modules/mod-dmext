/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "dmext_priv.h"
#include "dmext_assert.h"

static int isdot(int c) {
    if(c == '.') {
        return 1;
    }

    return 0;
}

static const amxc_var_t* dmext_get_transform_data(amxd_object_t* object, amxd_param_t* param, void* priv) {
    amxd_object_t* base = NULL;
    amxc_var_t* transform = (amxc_var_t*) priv;
    amxc_var_t* transform_validate = NULL;
    const char* param_name = amxd_param_get_name(param);

    // transformation data can be attached to
    // - write action "_set_object_ref"
    // - validate action "_check_is_object_ref"
    // of the given object.
    // Or when it is an instance it can be attached to the template object callback functions
    // Or it can be inherited from the base object.

    // If no transformation data is provided in the private data
    if(transform == NULL) {
        // fetch it from the write action or the validate action
        transform = amxd_param_get_action_cb_data(param, action_param_write, _set_object_ref);
        transform_validate = amxd_param_get_action_cb_data(param, action_param_validate, _check_is_object_ref);
    }

    if((transform == NULL) && (transform_validate == NULL)) {
        // Check the parent or base object depening on the object type of the given object
        if((amxd_object_get_type(object) == amxd_object_instance)) {
            // if it is an instance object use the template object
            base = amxd_object_get_parent(object);
        } else {
            // otherwise fetch the base object
            base = amxd_object_get_base(object);
        }
        // Loop over the base objects until we find the transform data
        // or until we reach the top object of the inheritance chain
        while(transform == NULL && transform_validate == NULL && base != NULL) {
            amxd_param_t* def = amxd_object_get_param_def(base, param_name);
            if(transform == NULL) {
                transform = amxd_param_get_action_cb_data(def, action_param_write, _set_object_ref);
            }
            if(transform_validate == NULL) {
                transform_validate = amxd_param_get_action_cb_data(def, action_param_validate, _check_is_object_ref);
            }
            base = amxd_object_get_base(base);
        }
    }

    return transform != NULL ? transform : transform_validate;
}

static const amxc_var_t* dmext_get_transform(const char* path, const amxc_var_t* transform) {
    amxc_var_t* transform_var = NULL;
    amxc_var_for_each(t, transform) {
        const char* key = amxc_var_key(t);
        size_t len = strlen(key);

        if((strncmp(path, key, len) == 0) ||
           (strncmp(path, key, len - 1) == 0)) {
            transform_var = t;
        }
    }

    return transform_var;
}

static char* dmext_transform_path(const char* path, const amxc_var_t* transform_var) {
    char* new_path = NULL;
    amxc_string_t tpath;

    amxc_string_init(&tpath, 0);
    when_null(transform_var, exit);

    amxc_string_set(&tpath, path);
    amxc_string_append(&tpath, ".", 1);
    amxc_string_replace(&tpath, "..", ".", UINT32_MAX);
    amxc_string_replace(&tpath, amxc_var_key(transform_var), GET_CHAR(transform_var, NULL), 1);
    amxc_string_trim(&tpath, isdot);
    new_path = amxc_string_take_buffer(&tpath);

exit:
    amxc_string_clean(&tpath);
    return new_path;
}

static void dmext_reference_remove(amxd_param_t* param, const char* path, const amxc_var_t* transform) {
    amxd_object_t* obj = amxd_param_get_owner(param);
    amxc_var_t old_values;
    amxc_var_t paths;

    amxc_var_init(&old_values);
    amxc_var_init(&paths);

    amxd_object_get_params(obj, &old_values, amxd_dm_access_protected);

    amxc_var_convert(&paths, &param->value, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&paths, AMXC_VAR_ID_LIST);

    amxc_var_for_each(p, &paths) {
        char* tpath = NULL;
        const char* stored_path = GET_CHAR(p, NULL);
        if(transform != NULL) {
            const amxc_var_t* transform_var = dmext_get_transform(stored_path, transform);
            tpath = dmext_transform_path(stored_path, transform_var);
        }
        if(strcmp((tpath != NULL) ? tpath : stored_path, path) == 0) {
            amxc_var_delete(&p);
        }
        free(tpath);
    }

    amxc_var_convert(&param->value, &paths, amxd_param_get_type(param));

    amxd_object_emit_changed(obj, &old_values);

    amxc_var_clean(&old_values);
    amxc_var_clean(&paths);
}

static void dmext_reference_deleted(const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    amxd_param_t* param = (amxd_param_t*) priv;
    amxd_object_t* object = amxd_param_get_owner(param);
    amxc_llist_t* subscriptions = (amxc_llist_t*) param->priv;
    const char* notification = GET_CHAR(data, "notification");
    amxc_string_t path;
    const amxc_var_t* transform = dmext_get_transform_data(object, param, NULL);
    notification = notification == NULL ? sig_name : notification;

    amxc_string_init(&path, 0);
    amxc_string_setf(&path, "%s", GET_CHAR(data, "path"));
    amxc_string_trimr(&path, isdot);

    // remove subscription
    amxc_llist_for_each(it, subscriptions) {
        amxb_subscription_t* subscription = amxc_container_of(it, amxb_subscription_t, it);
        // No need for NULL checks here.
        // This function can only be called when a instance-remove or object-delete
        // event occured. In both cases the field "notification" will be available
        // in the data variant.
        // The path string will not be empty or NULL as in the data variant at least
        // the path is available.
        if(strcmp(subscription->object, amxc_string_get(&path, 0)) == 0) {
            amxb_subscription_delete(&subscription);
        }
    }

    if(strcmp(notification, "dm:instance-removed") == 0) {
        amxc_string_setf(&path, "%s%d", GET_CHAR(data, "path"), GET_UINT32(data, "index"));
    } else {
        amxc_string_setf(&path, "%s", GET_CHAR(data, "path"));
        amxc_string_trimr(&path, isdot);
    }
    dmext_reference_remove(param, amxc_string_get(&path, 0), transform);

    amxc_string_clean(&path);
}

static int dmext_reference_subscribe(amxb_bus_ctx_t* bus_ctx,
                                     amxd_param_t* param,
                                     amxc_var_t* described) {
    amxc_llist_t* subscriptions = (amxc_llist_t*) param->priv;
    amxd_dm_t* dm = amxd_object_get_dm(amxd_param_get_owner(param));
    amxb_subscription_t* subscription = NULL;
    amxd_object_type_t type = GET_UINT32(described, "type_id");
    amxc_string_t expr;
    const char* path = NULL;
    const char* signal = "dm:instance-removed";
    int rv = -1;

    amxc_string_init(&expr, 0);
    when_null(subscriptions, exit);

    path = amxc_var_constcast(cstring_t, GET_ARG(described, "path"));

    if(type == amxd_object_instance) {
        uint32_t index = amxc_var_dyncast(uint32_t, GET_ARG(described, "index"));
        if(bus_ctx != NULL) {
            amxc_string_setf(&expr, "notification == '%s' && path == '%s' && index == %d",
                             signal, path, index);
        } else {
            amxc_string_setf(&expr, "path == '%s' && index == %d",
                             path, index);
        }
    } else {
        signal = "dm:object-removed";
        if(bus_ctx != NULL) {
            amxc_string_setf(&expr, "notification == '%s' && path == '%s'",
                             signal, path);
        } else {
            amxc_string_setf(&expr, "path == '%s'", path);
        }
    }

    if(bus_ctx != NULL) {
        rv = amxb_subscription_new(&subscription, bus_ctx, path, amxc_string_get(&expr, 0),
                                   dmext_reference_deleted, param);
        when_failed(rv, exit);
        amxc_llist_append(subscriptions, &subscription->it);
    } else {
        amxp_slot_connect(&dm->sigmngr, signal, amxc_string_get(&expr, 0),
                          dmext_reference_deleted, param);
    }

exit:
    amxc_string_clean(&expr);
    return rv;
}

static amxd_status_t dmext_reference_get(amxd_dm_t* dm,
                                         const char* path,
                                         const amxc_var_t* transform_var,
                                         amxc_var_t* described,
                                         amxb_bus_ctx_t** bus_ctx) {
    amxd_status_t status = amxd_status_invalid_path;
    int rv = -1;
    char* tpath = NULL;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    tpath = dmext_transform_path(path, transform_var);

    ctx = amxb_be_who_has(tpath != NULL ? tpath : path);
    if(ctx != NULL) {
        rv = amxb_describe(ctx, tpath != NULL ? tpath : path, 0, &ret, 5);
        if(rv == 0) {
            status = amxd_status_ok;
            amxc_var_set_type(described, AMXC_VAR_ID_HTABLE);
            amxc_var_move(described, GETI_ARG(&ret, 0));
            *bus_ctx = ctx;
        }
    } else {
        amxd_object_t* object = amxd_dm_findf(dm, "%s", tpath != NULL ? tpath : path);
        when_null(object, exit);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        status = amxd_object_invoke_function(object, "_describe", &args, described);
    }

exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    free(tpath);
    return status;
}

static void dmext_reference_get_path(amxc_var_t* described,
                                     const amxc_var_t* transform_var,
                                     amxc_var_t* path) {
    amxc_string_t str_path;
    amxd_object_type_t type
        = (amxd_object_type_t) amxc_var_dyncast(uint32_t,
                                                GET_ARG(described, "type_id"));
    const char* p = amxc_var_constcast(cstring_t, GET_ARG(described, "path"));
    const char* key = amxc_var_key(transform_var);
    const char* value = GET_CHAR(transform_var, NULL);

    amxc_string_init(&str_path, 0);

    if(type == amxd_object_instance) {
        uint32_t index = amxc_var_dyncast(uint32_t, GET_ARG(described, "index"));
        amxc_string_setf(&str_path, "%s%d", p, index);
    } else {
        amxc_string_setf(&str_path, "%s", p);
    }

    if(key != NULL) {
        amxc_string_prepend(&str_path, ".", 1);
        if(*value != 0) {
            amxc_string_replace(&str_path, value, key, 1);
        } else {
            amxc_string_prepend(&str_path, key, strlen(key));
        }
        amxc_string_replace(&str_path, "..", ".", UINT32_MAX);
    }
    amxc_string_trim(&str_path, isdot);

    amxc_var_push(cstring_t, path, amxc_string_take_buffer(&str_path));
    amxc_string_clean(&str_path);
}

static amxd_status_t object_reference_normalize(amxd_object_t* object,
                                                amxd_param_t* param,
                                                const amxc_var_t* transform,
                                                amxc_var_t* paths) {
    amxd_status_t status = amxd_status_unknown_error;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t described;
    amxd_dm_t* dm = amxd_object_get_dm(object);

    amxc_var_init(&described);

    amxc_var_for_each(p, paths) {
        const amxc_var_t* transform_var = NULL;
        const char* path = GET_CHAR(p, NULL);
        if(transform != NULL) {
            transform_var = dmext_get_transform(path, transform);
        }
        status = dmext_reference_get(dm, path, transform_var, &described, &ctx);
        when_failed(status, exit);
        // update path in list - make sure it is an index path, not dot terminated
        dmext_reference_get_path(&described, transform_var, p);
        // take subscription to monitor referenced object
        dmext_reference_subscribe(ctx, param, &described);
    }

exit:
    amxc_var_clean(&described);
    return status;
}

static amxd_status_t object_reference_exists(amxb_bus_ctx_t* ctx, amxd_dm_t* dm, const char* path) {
    amxd_status_t status = amxd_status_ok;
    // While starting up (before entry-points are called) it is possible that
    // the referenced object is present in local dm but not registered to a bus system.
    // Therefor it is needed to check if the object is present in the local dm.
    if(ctx == NULL) {
        if(amxd_dm_findf(dm, "%s", path) == NULL) {
            status = amxd_status_invalid_path;
        }
    } else {
        amxc_var_t ret;
        amxc_var_init(&ret);
        if(amxb_describe(ctx, path, AMXB_FLAG_EXISTS, &ret, 5) != 0) {
            status = amxd_status_invalid_path;
        } else {
            if(!GETI_BOOL(&ret, 0)) {
                status = amxd_status_invalid_path;
            }
        }
        amxc_var_clean(&ret);
    }

    return status;
}

static int is_dot(int c) {
    return (c == '.') ? 1 : 0;
}

amxd_status_t _check_is_object_ref(amxd_object_t* object,
                                   amxd_param_t* param,
                                   amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   amxc_var_t* const retval,
                                   void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* transform = NULL;
    amxc_var_t paths;
    amxd_dm_t* dm = amxd_object_get_dm(object);

    amxc_var_init(&paths);
    when_true_status(reason != action_param_validate &&
                     reason != action_describe_action,
                     exit,
                     status = amxd_status_function_not_implemented);

    status = amxd_action_describe_action(reason, retval, "check_is_object_ref", priv);
    when_true(status == amxd_status_ok, exit);

    transform = dmext_get_transform_data(object, param, priv);

    status = amxd_action_param_validate(object, param, reason, args, retval, priv);
    when_failed(status, exit);

    // convert string to list of strings
    amxc_var_convert(&paths, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&paths, AMXC_VAR_ID_LIST);

    status = amxd_status_ok;

    // check each individual path in list
    amxc_var_for_each(p, &paths) {
        char* tpath = NULL;
        const char* path = GET_CHAR(p, NULL);
        amxb_bus_ctx_t* ctx = NULL;
        if(transform != NULL) {
            const amxc_var_t* transform_var = dmext_get_transform(path, transform);
            tpath = dmext_transform_path(path, transform_var);
        }
        ctx = amxb_be_who_has((tpath != NULL) ? tpath : path);
        status = object_reference_exists(ctx, dm, (tpath != NULL) ? tpath : path);
        free(tpath);
        if(status != amxd_status_ok) {
            break;
        }
    }

exit:
    amxc_var_clean(&paths);
    return status;
}

amxd_status_t _read_object_ref(amxd_object_t* object,
                               amxd_param_t* param,
                               amxd_action_t reason,
                               const amxc_var_t* const args,
                               amxc_var_t* const retval,
                               void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* transform = NULL;
    uint32_t access = GET_UINT32(args, "access");

    when_true_status(reason != action_param_read,
                     exit,
                     status = amxd_status_function_not_implemented);

    // when access is protected or public return the real value
    // use the default parameter read action method
    if(access != amxd_dm_access_private) {
        status = amxd_action_param_read(object, param, reason, args, retval, priv);
        goto exit;
    }

    transform = dmext_get_transform_data(object, param, priv);

    // convert string to list of strings
    amxc_var_convert(retval, &param->value, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(retval, AMXC_VAR_ID_LIST);

    status = amxd_status_ok;

    // check each individual path in list
    amxc_var_for_each(p, retval) {
        const char* path = GET_CHAR(p, NULL);
        if(transform != NULL) {
            const amxc_var_t* transform_var = dmext_get_transform(path, transform);
            char* tpath = dmext_transform_path(path, transform_var);
            amxc_var_push(cstring_t, p, tpath);
            tpath = NULL;
        }
    }

    amxc_var_cast(retval, amxc_var_type_of(&param->value));

exit:
    return status;
}

amxd_status_t _set_normalized_object_ref(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_var_t* transform = NULL;
    amxc_var_t described;
    amxc_var_t paths;

    amxc_var_init(&paths);
    amxc_var_init(&described);

    when_null(param, exit);
    when_true_status(reason != action_param_write,
                     exit,
                     status = amxd_status_function_not_implemented);

    transform = dmext_get_transform_data(object, param, priv);

    // convert string to list of strings
    amxc_var_convert(&paths, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&paths, AMXC_VAR_ID_LIST);

    object_reference_normalize(object, param, transform, &paths);

    // convert back to string, to be able to store it in the data model
    amxc_var_cast(&paths, AMXC_VAR_ID_CSV_STRING);

    status = amxd_action_param_write(object, param, reason, &paths, retval, priv);
    when_failed(status, exit);

    // This write action handler can transform the incoming object ref path from
    // key addressing into index address and can remove the trailing dot.
    // To make clear what was written, the transformed data is stored back in
    // the incoming argument, to be able to do that the const must be casted
    // away, this is in general not a good thing to do. But it is safe to
    // change the value of a amxc_var_t, the only side effect here is that all
    // next write handlers get the new value.
    amxc_var_convert((amxc_var_t*) args, &paths, amxc_var_type_of(args));

exit:
    amxc_var_clean(&described);
    amxc_var_clean(&paths);
    return status;
}

amxd_status_t _set_object_ref(amxd_object_t* object,
                              amxd_param_t* param,
                              amxd_action_t reason,
                              const amxc_var_t* const args,
                              amxc_var_t* const retval,
                              void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_llist_t* subscriptions = NULL;
    amxd_dm_t* dm = amxd_object_get_dm(object);

    when_null(param, exit);
    when_true_status(reason != action_param_write,
                     exit,
                     status = amxd_status_function_not_implemented);

    subscriptions = (amxc_llist_t*) param->priv;
    if(subscriptions == NULL) {
        amxc_llist_new(&subscriptions);
        param->priv = subscriptions;
    } else {
        amxc_llist_clean(subscriptions, amxb_subscription_remove_it);
        amxp_slot_disconnect_with_priv(&dm->sigmngr, dmext_reference_deleted, param);
    }

    status = _set_normalized_object_ref(object, param, reason, args, retval, priv);

exit:
    return status;
}

amxd_status_t _set_object_ref_simple(amxd_object_t* object,
                                     amxd_param_t* param,
                                     amxd_action_t reason,
                                     const amxc_var_t* const args,
                                     amxc_var_t* const retval,
                                     void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t paths;
    amxc_string_t stripped_path;

    amxc_string_init(&stripped_path, 0);
    amxc_var_init(&paths);

    when_null(param, exit);
    when_true_status(reason != action_param_write,
                     exit,
                     status = amxd_status_function_not_implemented);

    // convert string to list of strings
    amxc_var_convert(&paths, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&paths, AMXC_VAR_ID_LIST);

    amxc_var_for_each(path, &paths) {
        amxc_string_setf(&stripped_path, "%s", amxc_var_constcast(cstring_t, path));
        amxc_string_trimr(&stripped_path, is_dot);
        amxc_var_set(cstring_t, path, amxc_string_get(&stripped_path, 0));
    }

    // convert back to string, to be able to store it in the data model
    amxc_var_cast(&paths, AMXC_VAR_ID_CSV_STRING);

    status = amxd_action_param_write(object, param, reason, &paths, retval, priv);
    when_failed(status, exit);

    // This write action handler can remove the trailing dot from a ref path.
    // To make clear what was written, the transformed data is stored back in
    // the incoming argument. To be able to do that the const must be casted
    // away. This is in general not a good thing to do, but it is safe to
    // change the value of a amxc_var_t, the only side effect here is that all
    // next write handlers get the new value.
    amxc_var_convert((amxc_var_t*) args, &paths, amxc_var_type_of(args));

exit:
    amxc_string_clean(&stripped_path);
    amxc_var_clean(&paths);
    return status;
}


amxd_status_t _destroy_object_ref(UNUSED amxd_object_t* object,
                                  amxd_param_t* param,
                                  amxd_action_t reason,
                                  UNUSED const amxc_var_t* const args,
                                  amxc_var_t* const retval,
                                  UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    amxc_llist_t* subscriptions = NULL;
    amxd_dm_t* dm = amxd_object_get_dm(object);

    when_null(param, exit);
    when_true_status(reason != action_param_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    subscriptions = (amxc_llist_t*) param->priv;

    amxc_llist_delete(&subscriptions, amxb_subscription_remove_it);
    amxp_slot_disconnect_with_priv(&dm->sigmngr, dmext_reference_deleted, param);
    param->priv = NULL;

    amxc_var_clean(retval);

exit:
    return status;
}
