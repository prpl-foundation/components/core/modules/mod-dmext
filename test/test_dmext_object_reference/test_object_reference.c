/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "dmext_priv.h"
#include "test_object_reference.h"

#include <amxb/amxb_register.h>
#include <amxd/amxd_transaction.h>

#include "../mocks/dummy_be.h"

static amxo_parser_t parser;
static amxd_dm_t dm;

static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_object_reference_setup(UNUSED void** state) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxd_object_t* root_obj = NULL;

    amxo_parser_init(&parser);
    amxd_dm_init(&dm);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "check_is_object_ref", AMXO_FUNC(_check_is_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "set_object_ref", AMXO_FUNC(_set_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "read_object_ref", AMXO_FUNC(_read_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "destroy_object_ref", AMXO_FUNC(_destroy_object_ref)), 0);
    assert_int_equal(amxo_resolver_ftab_add(&parser, "set_object_ref_simple", AMXO_FUNC(_set_object_ref_simple)), 0);

    root_obj = amxd_dm_get_root(&dm);
    amxo_parser_parse_file(&parser, "./test.odl", root_obj);

    assert_int_equal(test_register_dummy_be(), 0);
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxb_register(bus_ctx, &dm);
    amxb_set_access(bus_ctx, amxd_dm_access_public);

    handle_events();

    assert_int_equal(test_load_dummy_remote("./greeter_definition.odl"), 0);

    return 0;
}

int test_object_reference_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();

    return 0;
}

void test_can_set_valid_reference(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_var_t data;

    amxc_var_init(&data);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Greeter.History.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "Reference", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.1");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference1", "Device.Greeter.History.1.");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference2", "Device.Greeter.History.1");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference3", "Device.Greeter.History.1.SubObject.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    assert_int_equal(amxd_object_get_param(object, "TransformedReference1", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.1");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference2", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Device.Greeter.History.1");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference3", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Device.Greeter.History.1.SubObject");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject.MITestObject.1");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference1", "Device.Greeter.History.[Alias == 'cpe-History-2'].");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference2", "Device.Greeter.History.[Alias == 'cpe-History-1']");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference3", "Device.Greeter.History.cpe-History-1.SubObject.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.MITestObject.1.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "TransformedReference1", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.2");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference2", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Device.Greeter.History.1");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference3", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Device.Greeter.History.1.SubObject");

    amxc_var_clean(&data);
}

void test_read_reference_is_transformed(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "TestObject");
    amxc_var_t params;

    amxc_var_init(&params);
    assert_non_null(object);

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_private), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Greeter.History.1");

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Device.Greeter.History.1");

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_public), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Device.Greeter.History.1");

    amxc_var_clean(&params);
}

void test_is_updated_when_ref_obj_is_removed(UNUSED void** state) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("Greeter.");
    amxc_var_t ret;
    amxd_object_t* object = NULL;
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_init(&ret);

    amxb_del(ctx, "Greeter.History.1.", 0, NULL, &ret, 5);
    amxc_var_clean(&ret);
    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.MITestObject.1.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "TransformedReference1", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.2");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference2", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "");
    assert_int_equal(amxd_object_get_param(object, "TransformedReference3", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "");

    amxc_var_clean(&data);
}

void test_can_set_valid_references(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_var_t data;
    amxd_param_t* param = NULL;

    amxc_var_init(&data);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Greeter.History.2.,Greeter.,Greeter.History.[Alias == 'cpe-History-2'].Info.Info1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "Reference", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.2,Greeter,Greeter.History.2.Info.1");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference1", "Device.Greeter.History.2.,Device.Greeter.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    param = amxd_object_get_param_def(object, "TransformedReference1");
    assert_non_null(param);
    assert_int_equal(amxd_object_get_param(object, "TransformedReference1", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.2,Greeter");
    assert_string_equal(GET_CHAR(&param->value, NULL), "Device.Greeter.History.2,Device.Greeter");

    amxc_var_clean(&data);
}

void test_can_clear_references(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_var_t data;

    amxc_var_init(&data);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Greeter.History.2.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "Reference", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.2");

    amxc_var_clean(&data);

    amxc_var_init(&data);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "Reference", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "");

    amxc_var_clean(&data);
}

void test_read_csv_reference_is_transformed(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "TestObject");
    amxc_var_t params;

    amxc_var_init(&params);
    assert_non_null(object);

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_private), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Greeter.History.2,Greeter");

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Device.Greeter.History.2,Device.Greeter");

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_public), 0);
    amxc_var_dump(&params, STDOUT_FILENO);
    assert_string_equal(GET_CHAR(&params, "TransformedReference1"), "Device.Greeter.History.2,Device.Greeter");

    amxc_var_clean(&params);
}

void test_set_invalid_reference(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference2", "Dummy.NotExisting.2.");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference", "Greeter.History.99.");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference2", "Device.Greeter.History.99.");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "TransformedReference2", "Device.Greeter.[Alias=='NotExisting'].");
    assert_int_not_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();
}

void test_can_set_valid_reference_simple(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;
    amxc_var_t data;

    amxc_var_init(&data);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "TestObject");
    amxd_trans_set_value(cstring_t, &trans, "Reference2", "Greeter.History.1.");
    assert_int_equal(amxd_trans_apply(&trans, &dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    object = amxd_dm_findf(&dm, "TestObject.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_param(object, "Reference2", &data), 0);
    assert_string_equal(GET_CHAR(&data, NULL), "Greeter.History.1");

    amxc_var_clean(&data);
}
